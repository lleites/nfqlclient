#!/usr/bin/env nodejs

var assert = require('assert');
var restify = require('restify');

var program = require('commander');

program.version('1.0.0')
    .option('-s, --server [server]', 'FQL server.', 'http://tomcatdemo-jfql.rhcloud.com')
    .option('-q, --query [command]', 'FQL query.', 'show forms')
    .parse(process.argv);
    

var NFQLClient = {};
 
NFQLClient.client = restify.createJsonClient({
  url: program.server,
  version: '~1.0'
});

NFQLClient.command = program.query;
NFQLClient.command = NFQLClient.command.replace(' ','%20');
 
NFQLClient.client.get('/appbase-webconsole/json?command=' + NFQLClient.command, function (err, req, res, obj) {
  assert.ifError(err);
  console.log('Server returned: ');
  var jsonResult = JSON.stringify(obj, undefined, 5).split('\n');
  for (var i in jsonResult) {
    console.log(jsonResult[i]);
  }
  
  NFQLClient.client.close();
});
